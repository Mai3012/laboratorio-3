class AddDescripcionToMonstruo < ActiveRecord::Migration[5.2]
  def change
    add_column :monstruos, :descripcion, :string
  end
end
